package org.cahsieh.nfclockfolder.Widget;

import android.content.Context;
import android.support.v4.content.FileProvider;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;

/**
 * Created by CAMac on 2016/6/7.
 */
public class Connecter {

    private static final String HOST_IP ="140.120.**.**";
    private static final int PORT =5566;

    private static Connecter instance = null;

    public static Connecter getInstance(){
        if(instance == null){
            synchronized (Connecter.class){
                if(instance == null){
                    instance = new Connecter();
                }
            }
        }
        return instance;
    }

    private Connecter(){
    }

    public String getList(String folderName){
        String command = "GET_LIST&"+folderName;
        Log.v("Command",command);
        Socket socket = new Socket();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(HOST_IP,PORT);
        try {
            socket.connect(inetSocketAddress);
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            outputStream.write(command.getBytes("UTF-8"));
            outputStream.flush();
            socket.shutdownOutput();
            BufferedInputStream inputStream = new BufferedInputStream(socket.getInputStream());
            int len;
            byte[] buffer = new byte[1024];
            String data = "";
            while((len = inputStream.read(buffer)) > 0){
                data = data + new String(buffer,0,len,"UTF-8");
            }
            Log.v("GET_LIST",data);
            outputStream.close();
            inputStream.close();
            socket.close();
            return data;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getFile(Context context,String fileName){
        String command = "GET_FILE&"+fileName;
        Log.v("Command",command);
        Socket socket = new Socket();
        InetSocketAddress inetSocketAddress = new InetSocketAddress(HOST_IP,PORT);
        try {
            socket.connect(inetSocketAddress);
            DataOutputStream outputStream = new DataOutputStream(socket.getOutputStream());
            outputStream.write(command.getBytes("UTF-8"));
            outputStream.flush();
            socket.shutdownOutput();
            BufferedInputStream inputStream = new BufferedInputStream(socket.getInputStream());
            int len;
            byte[] buffer = new byte[1024];
            File path = new File(context.getFilesDir(),"tmp");
            if(!path.exists()){
                path.mkdir();
            }
            File file = new File(path,fileName);
            BufferedOutputStream fileOutStream = new BufferedOutputStream(new FileOutputStream(file));
            while((len = inputStream.read(buffer)) > 0){
                fileOutStream.write(buffer,0,len);
            }
            fileOutStream.flush();
            fileOutStream.close();
            Log.v("GET_FILE","File Path: "+file.getAbsolutePath());
            outputStream.close();
            inputStream.close();
            socket.close();
            return file.getAbsolutePath();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

}
