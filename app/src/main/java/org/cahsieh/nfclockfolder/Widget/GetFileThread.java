package org.cahsieh.nfclockfolder.Widget;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by CAMac on 2016/6/8.
 */
public class GetFileThread extends Thread{

    private Context context;
    private Handler handler;
    private String FileName;

    public GetFileThread(Context context,Handler handler,String FileName){
        this.context = context;
        this.handler = handler;
        this.FileName = FileName;
    }

    @Override
    public void run() {
        String path = Connecter.getInstance().getFile(context,FileName);
        Bundle bundle = new Bundle();
        bundle.putString("TASK","FILE");
        bundle.putString("PATH",path);
        Message message = new Message();
        message.setData(bundle);
        handler.sendMessage(message);
    }
}
