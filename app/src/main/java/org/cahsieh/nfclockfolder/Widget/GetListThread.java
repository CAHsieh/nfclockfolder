package org.cahsieh.nfclockfolder.Widget;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

/**
 * Created by CAMac on 2016/6/7.
 */
public class GetListThread extends Thread {

    private Handler handler;
    private String FolderName;

    public GetListThread(Handler handler){
        this(handler,"");
    }

    public GetListThread(Handler handler,String FolderName){
        this.handler = handler;
        this.FolderName = FolderName;
    }

    @Override
    public void run() {
        String firstlayer = Connecter.getInstance().getList(FolderName);
        Bundle bundle = new Bundle();
        bundle.putString("TASK","LIST");
        bundle.putString("List",firstlayer);
        Message message = new Message();
        message.setData(bundle);
        handler.sendMessage(message);
    }
}
