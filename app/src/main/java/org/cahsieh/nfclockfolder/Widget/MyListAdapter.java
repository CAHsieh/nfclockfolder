package org.cahsieh.nfclockfolder.Widget;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.cahsieh.nfclockfolder.R;

/**
 * Created by CAMac on 2016/6/7.
 */
public class MyListAdapter extends BaseAdapter {

    private Context context;
    private LayoutInflater inflater;
    private String[] Data;

    public MyListAdapter(Context context, String[] Data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.Data = Data;
    }

    public void setData(String[] Data) {
        this.Data = Data;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return Data.length;
    }

    @Override
    public Object getItem(int position) {
        return Data[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        if (convertView == null) {
            convertView = inflater.inflate(android.R.layout.simple_list_item_1, null);
        }
        textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setTextColor(Color.BLACK);
        String[] data = Data[position].split("\\?");
        textView.setText(data[0]);
        textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelOffset(R.dimen.one_grid_size));
        if (data[1].equals("file")) {
            textView.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_insert_drive_file_black_48dp), null, null, null);
        } else if (data[1].equals("folder")) {
            textView.setCompoundDrawablesWithIntrinsicBounds(ContextCompat.getDrawable(context, R.drawable.ic_folder_black_48dp), null, null, null);
        }
        return convertView;
    }
}
