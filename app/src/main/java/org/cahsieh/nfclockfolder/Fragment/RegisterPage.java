package org.cahsieh.nfclockfolder.Fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.cahsieh.nfclockfolder.R;

/**
 * Created by CAMac on 2016/6/6.
 */
public class RegisterPage extends Fragment {

    private EditText pwd;
    private TextView title, id;
    private Button register;
    private String ID, password;
    private OnRegisterListener listener;

    public RegisterPage(){}

    public void setOnRegisterListener(OnRegisterListener listener) {
        this.listener = listener;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_registerpage, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pwd = (EditText) view.findViewById(R.id.Register_edt_pwd);
        title = (TextView) view.findViewById(R.id.Register_txt_Title);
        id = (TextView) view.findViewById(R.id.Register_txt_ID);
        register = (Button) view.findViewById(R.id.Register_btn_register);
        view.findViewById(R.id.Register_btn_pwdConfirm).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                password = pwd.getText().toString();
                if (password.length() < 4) {
                    Toast.makeText(getContext(), "Please Enter More Then 4 Numbers.", Toast.LENGTH_SHORT).show();
                } else {
                    pwd.setEnabled(false);
                    v.setEnabled(false);
                    title.setVisibility(View.VISIBLE);
                    id.setVisibility(View.VISIBLE);
                    register.setVisibility(View.VISIBLE);
                }
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ID.equals("")) {
                    Toast.makeText(getContext(), "Please Tag Your EasyCard.", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPreferences.Editor editor = getContext().getSharedPreferences("NCFLockFolder", Context.MODE_PRIVATE).edit();
                    editor.putString("NFCID", ID);
                    editor.putString("Password", password);
                    editor.commit();
                    if (listener != null) {
                        listener.OnRegister(password,ID);
                    }
                }
            }
        });
    }

    public void setID(String ID) {
        id.setText("ID:\t" + ID);
        this.ID = ID;
        register.setEnabled(true);
    }

    public interface OnRegisterListener {
        void OnRegister(String password,String NCFID);
    }
}