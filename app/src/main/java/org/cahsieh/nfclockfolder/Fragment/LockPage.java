package org.cahsieh.nfclockfolder.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.cahsieh.nfclockfolder.Activity.ContentActivity;
import org.cahsieh.nfclockfolder.R;

/**
 * Created by CAMac on 2016/6/6.
 */
public class LockPage extends Fragment implements View.OnClickListener {

    private EmergencyPage emergencyPage;
    private String password, TagID;
    private String inputpwd;
    private TextView pwdView;
    private int wrong_times;

    public LockPage() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        password = getArguments().getString("password", "");
        TagID = getArguments().getString("NFCID", "");

        return inflater.inflate(R.layout.fragment_lockpage, null);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pwdView = (TextView) view.findViewById(R.id.Lock_txt_password);
        view.findViewById(R.id.Lock_btn_0).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_1).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_2).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_3).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_4).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_5).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_6).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_7).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_8).setOnClickListener(this);
        view.findViewById(R.id.Lock_btn_9).setOnClickListener(this);
        inputpwd = "";
        wrong_times = 0;

        view.findViewById(R.id.Lock_btn_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                inputpwd = inputpwd.substring(0, inputpwd.length() - 1);
                pwdView.setText(inputpwd);
            }
        });

        view.findViewById(R.id.Lock_btn_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clearPWD();
            }
        });
    }

    private void clearPWD() {
        inputpwd = "";
        pwdView.setText(inputpwd);
    }

    public void setTagID(String ID) {
        if (emergencyPage == null) {
            emergencyPage = (EmergencyPage) getChildFragmentManager().findFragmentByTag("dialog");
        }
        if (TagID.equals(ID) && emergencyPage != null) {
            emergencyPage.stop();
            emergencyPage.dismiss();
            emergencyPage = null;
            wrong_times = 0;
        } else if (TagID.equals(ID) && emergencyPage == null) {
            getIN();
        } else {
            Toast.makeText(getContext(), "Wrong Tag!!!", Toast.LENGTH_SHORT).show();
        }
    }

    private void getIN() {
        Toast.makeText(getContext(), "Welcome", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent();
        intent.setClass(getActivity(), ContentActivity.class);
        startActivity(intent);
        getActivity().finish();
    }

    @Override
    public void onClick(View v) {
        inputpwd = inputpwd + ((Button) v).getText();
        pwdView.setText(inputpwd);
        if (inputpwd.length() == password.length()) {
            if (inputpwd.equals(password)) {
                getIN();
            } else {
                clearPWD();
                wrong_times++;
                if (wrong_times > 3) {
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    Fragment prev = getChildFragmentManager().findFragmentByTag("dialog");
                    if (prev != null) {
                        transaction.remove(prev);
                    }
                    emergencyPage = new EmergencyPage();
                    emergencyPage.setCancelable(false);
                    emergencyPage.show(transaction, "dialog");
                } else
                    Toast.makeText(getContext(), "Wrong Password " + wrong_times, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
