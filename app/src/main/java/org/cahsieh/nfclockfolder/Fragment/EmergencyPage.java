package org.cahsieh.nfclockfolder.Fragment;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.cahsieh.nfclockfolder.R;

/**
 * Created by CAMac on 2016/6/7.
 */
public class EmergencyPage extends DialogFragment {

    private MediaPlayer player;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(STYLE_NO_TITLE, 0);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_emergency, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        player = MediaPlayer.create(getContext(),R.raw.emergency);
        player.start();
        player.setLooping(true);
    }

    public void stop(){
        player.setLooping(false);
        player.stop();
        player.release();
    }
}
