package org.cahsieh.nfclockfolder.Activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.cahsieh.nfclockfolder.R;
import org.cahsieh.nfclockfolder.Widget.GetFileThread;
import org.cahsieh.nfclockfolder.Widget.GetListThread;
import org.cahsieh.nfclockfolder.Widget.MyListAdapter;

import java.io.File;

public class ContentActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private ProgressBar progressBar;
    private ListView listView;
    private MyListAdapter adapter;
    private Button refresh;

    private String currentFolder = "";
    private File file;
    private final String[] backItem = new String[]{"..?back"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        refresh = (Button) findViewById(R.id.Content_btn_refresh);
        assert refresh != null;
        refresh.setOnClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.Content_progress);

        listView = (ListView) findViewById(R.id.Content_List);
        adapter = new MyListAdapter(getApplicationContext(), new String[]{});
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        new GetListThread(handler).start();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (file != null && file.exists()) {
            file.delete();
        }
    }

    @Override
    public void onBackPressed() {
        if (backOrLeave()) {
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            startActivity(intent);
            finish();
        }
    }

    private boolean backOrLeave() {
        if (currentFolder.contains("\\")) {
            currentFolder = currentFolder.substring(0, currentFolder.lastIndexOf("\\"));
            new GetListThread(handler, currentFolder).start();
            return false;
        } else if (!currentFolder.equals("")) {
            currentFolder = "";
            new GetListThread(handler).start();
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onClick(View v) {
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        refresh.setVisibility(View.GONE);
        new GetListThread(handler, currentFolder).start();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String[] data = ((String) listView.getItemAtPosition(position)).split("\\?");
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
        refresh.setVisibility(View.GONE);
        switch (data[1]) {
            case "file":
                String clickFileName = "";
                if (currentFolder.equals(""))
                    clickFileName = data[0];
                else
                    clickFileName = currentFolder + "\\" + data[0];
                new GetFileThread(getApplicationContext(), handler, clickFileName).start();
                break;
            case "folder":
                if (currentFolder.equals("")) {
                    currentFolder = currentFolder + data[0];
                } else {
                    currentFolder = currentFolder + "\\" + data[0];
                }
                new GetListThread(handler, currentFolder).start();
                break;
            case "back":
                backOrLeave();
                break;
        }
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            progressBar.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
            refresh.setVisibility(View.VISIBLE);

            Bundle bundle = msg.getData();
            switch (bundle.getString("TASK", "")) {
                case "LIST":
                    String listString = bundle.getString("List", "");
                    if (!listString.equals("")) {
                        String[] Data;
                        if (currentFolder.equals("")) {
                            Data = listString.split("\\*");
                        } else {
                            String[] items = listString.split("\\*");
                            Data = new String[items.length + 1];
                            System.arraycopy(backItem, 0, Data, 0, 1);
                            System.arraycopy(items, 0, Data, 1, items.length);
                        }
                        adapter.setData(Data);
                    }
                    break;
                case "FILE":
                    String path = bundle.getString("PATH", "");
                    if (!path.equals("")) {
                        file = new File(path);
                        if (file.exists()) {
                            Intent intent = new Intent(Intent.ACTION_VIEW);
                            Uri fileUri = FileProvider.getUriForFile(getApplicationContext(), "org.cahsieh.nfclockfolder", file);
                            Log.v("File URI", fileUri.toString());
                            intent.setDataAndType(fileUri, getContentResolver().getType(fileUri));
                            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                            startActivity(intent);
                        } else {
                            Toast.makeText(getApplicationContext(), "File Not Exists", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "File Not Exists", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }
    };

}
