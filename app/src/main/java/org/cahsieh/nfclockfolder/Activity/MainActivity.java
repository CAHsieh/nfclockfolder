package org.cahsieh.nfclockfolder.Activity;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.nfc.NfcAdapter;
import android.nfc.NfcManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import org.cahsieh.nfclockfolder.Fragment.LockPage;
import org.cahsieh.nfclockfolder.Fragment.RegisterPage;
import org.cahsieh.nfclockfolder.R;

public class MainActivity extends AppCompatActivity implements RegisterPage.OnRegisterListener {

    private RegisterPage registerPage;
    private LockPage lockPage;
    private NfcAdapter nfcAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    @Override
    protected void onResume() {
        super.onResume();
        nfcAdapter = ((NfcManager) getSystemService(Context.NFC_SERVICE)).getDefaultAdapter();
        if (nfcAdapter == null || !nfcAdapter.isEnabled()) {
            //OpenNfC
            new AlertDialog.Builder(this).setTitle("請開啟NFC服務").setPositiveButton("確認", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Settings.ACTION_NFC_SETTINGS);
                    startActivityForResult(intent, 0);
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            }).show();
        } else {
            Intent intent = new Intent(this, MainActivity.class).addFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
            IntentFilter[] intentFilters = new IntentFilter[]{};
            nfcAdapter.enableForegroundDispatch(this, pendingIntent, intentFilters, null);

            SharedPreferences sharedPreferences = getSharedPreferences("NCFLockFolder", MODE_PRIVATE);
            String NFCID = sharedPreferences.getString("NFCID", "");
            if (NFCID.equals("")) {
                if (registerPage == null) {
                    // go register page.
                    registerPage = new RegisterPage();
                    registerPage.setOnRegisterListener(this);
                    FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                    transaction.add(R.id.Main_layout_content, registerPage);
                    transaction.commit();
                }
            } else if (lockPage == null) {
                // go lock page.
                String password = sharedPreferences.getString("Password", "");
                lockPage = new LockPage();
                Bundle bundle = new Bundle();
                bundle.putString("password", password);
                bundle.putString("NFCID", NFCID);
                lockPage.setArguments(bundle);
                FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
                transaction.add(R.id.Main_layout_content, lockPage);
                transaction.commit();
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        nfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (intent.hasExtra(NfcAdapter.EXTRA_ID)) {
            byte[] tagId = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
            int i, j, in;
            String[] hex = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};
            String out = "";

            for (j = 0; j < tagId.length; ++j) {
                in = (int) tagId[j] & 0xff;
                i = (in >> 4) & 0x0f;
                out += hex[i];
                i = in & 0x0f;
                out += hex[i];
            }

            Log.v("Tag ID", out);

            if (lockPage != null) {
                lockPage.setTagID(out);
            } else if (registerPage != null) {
                registerPage.setID(out);
            }
        }
    }

    @Override
    public void OnRegister(String password, String NFCID) {
        Toast.makeText(getApplicationContext(), "Register Successfully!", Toast.LENGTH_SHORT).show();
        lockPage = new LockPage();
        Bundle bundle = new Bundle();
        bundle.putString("password", password);
        bundle.putString("NFCID", NFCID);
        lockPage.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.Main_layout_content, lockPage);
        transaction.commit();
    }
}
